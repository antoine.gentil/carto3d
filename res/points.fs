/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
#version 150
in vec4 pointColor;
out vec4 color;

void main() {
  color = pointColor;
}
