#version 150

uniform mat4 matrix;
uniform mat4 matrixPoints;
uniform vec4 uniPointColor;

in vec3 position;
out vec4 pointColor;

void main()
{
    gl_Position = matrixPoints  *  vec4(position, 1.0);
    pointColor = uniPointColor;
}