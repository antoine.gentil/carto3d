package antgent.carto3d;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketTest {

    public static void main(String[] args) throws Exception {

        String hostname = "octopi.local";
        int portNumber = 30003;

        Socket socket = new Socket(hostname, portNumber);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String line;

        while( (line = in.readLine()) != null){

            String[] split = line.split(",");

            if(!split[1].equals("3")){
                continue;
            }

            String hexIdent = split[4];

            try {
                float lat = Float.parseFloat(split[14]);
                float lon = Float.parseFloat(split[15]);

                System.out.println(hexIdent + " " + lat + " " + lon);

            } catch (NumberFormatException e){
                continue;
            }



        }

    }

}
