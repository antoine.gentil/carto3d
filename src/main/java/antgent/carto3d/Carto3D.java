/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package antgent.carto3d;

import static antgent.carto3d.DemoUtils.*;
import static antgent.carto3d.IOUtils.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL32C.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.io.*;
import java.lang.Math;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.nio.*;
import java.nio.file.Files;
import java.util.*;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.joml.*;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;
import org.opengis.feature.simple.SimpleFeature;

/**
 * Renders a simple textured sphere using OpenGL 4.0 Core Profile.
 *
 * @author Kai Burjack
 */
public class Carto3D {

	static final int rings = 160;
	static final int sectors = 160;

	long window;
	int width = 1024;
	int height = 768;

	int vao, vaoCoast;
	int eboCoast;
	int sphereProgram, pointsProgram;
	int sphereProgram_inputPosition, pointsProgram_inputPosition;
	int sphereProgram_inputTextureCoords;
	int sphereProgram_matrixUniform, pointsProgram_matrixUniform;
	int pointsProgram_uniPointColor;

	Matrix4f m = new Matrix4f();
	Matrix4f mPoints = new Matrix4f();
	FloatBuffer matrixBuffer = MemoryUtil.memAllocFloat(16);
	FloatBuffer matrixRotBuffer = MemoryUtil.memAllocFloat(16);

	float time;

	GLCapabilities caps;
	GLFWKeyCallback keyCallback;
	GLFWFramebufferSizeCallback fbsCallback;
	Callback debugProc;
	private int pointsProgram_matRotUniform;
	private int vaoCities;
	private int vaoPlanes;

	List<Vector2f> planesPoints = Collections.synchronizedList(new ArrayList<>());
	private int planeVbo;
	private Thread thread;

	int planeIdx = 0;
	private int nbCities;
	private float cameraLat = 48.390394f;
	private float cameraLon = -4.486076f;

	private float camerady = 0.0f;
	private float cameradx = 0.0f;
	private GLFWCursorPosCallback posCallback;
	private double mouseX;
	private double mouseY;
	private GLFWMouseButtonCallback mouseCallback;
	private boolean dragging;

	private int lastMouseX, lastMouseY;

	private float cameraDist = 15.0f;
	private GLFWScrollCallback scrollCallback;
	private List<Integer> indices;

	class SocketThread implements Runnable {

		@Override
		public void run() {
			String hostname = "octopi.local";
			int portNumber = 30003;
			BufferedReader in = null;
			Socket socket = null;
			try {
				socket = new Socket(hostname, portNumber);
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			} catch (IOException e) {
				return;
			}

			String line;

			while (!Thread.interrupted()) {
				try {

					line = in.readLine();

					if (line == null) {
						break;
					}

					String[] split = line.split(",");

					if (!split[1].equals("3")) {
						continue;
					}

					String hexIdent = split[4];

					float lat = Float.parseFloat(split[14]);
					float lon = Float.parseFloat(split[15]);

					planesPoints.add(new Vector2f(lon, lat));

				} catch (Exception e) {
					continue;
				}
			}

			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	Vector3f coordsToCartesian(float lon, float lat, float alt) {

		float phi = (float) ((90.0 - lat) * (Math.PI / 180.0));
		float theta = (float) ((lon + 180.0) * (Math.PI / 180.0));

		float z = (float) (alt * Math.sin(phi) * Math.cos(theta));
		float x = (float) (alt * Math.sin(phi) * Math.sin(theta));
		float y = alt * (float) Math.cos(phi);

		return new Vector3f(x, y, z);
	}

	Vector3f coordsToCartesian(Vector2f coord, float alt) {

		return coordsToCartesian(coord.x, coord.y, alt);

	}

	void init() throws IOException {
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		window = glfwCreateWindow(width, height, "Simple textured sphere", NULL, NULL);
		glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
					glfwSetWindowShouldClose(window, true);
				if (key == GLFW_KEY_LEFT && action != GLFW_RELEASE)
					cameradx -= 1.0;
				if (key == GLFW_KEY_RIGHT && action != GLFW_RELEASE)
					cameradx += 1.0;
				if (key == GLFW_KEY_UP && action != GLFW_RELEASE)
					camerady += 1.0;
				if (key == GLFW_KEY_DOWN && action != GLFW_RELEASE)
					camerady -= 1.0;
			}
		});
		glfwSetFramebufferSizeCallback(window, fbsCallback = new GLFWFramebufferSizeCallback() {
			public void invoke(long window, int width, int height) {
				Carto3D.this.width = width;
				Carto3D.this.height = height;
			}
		});

		glfwSetCursorPosCallback(window, posCallback = GLFWCursorPosCallback.create((window, xpos, ypos) -> {
			Carto3D.this.mouseX = xpos;
			Carto3D.this.mouseY = ypos;
		}));

		glfwSetMouseButtonCallback(window,
				mouseCallback = GLFWMouseButtonCallback.create((window, button, action, mods) -> {

					if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
						Carto3D.this.dragging = true;
						lastMouseX = (int) mouseX;
						lastMouseY = (int) mouseY;
					} else if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
						Carto3D.this.dragging = false;
					}
				}));

		glfwSetScrollCallback(window, scrollCallback = GLFWScrollCallback.create((window, xoffset, yoffset) -> {
			if (cameraDist < 5.0f) {
				cameraDist += yoffset / 8.0f;
			} else {
				cameraDist += yoffset;
			}

			if (cameraDist < 1.1f)
				cameraDist = 1.1f;
			System.out.println(yoffset + " " + cameraDist);
		}));

		glfwMakeContextCurrent(window);
		try (MemoryStack frame = MemoryStack.stackPush()) {
			IntBuffer framebufferSize = frame.mallocInt(2);
			nglfwGetFramebufferSize(window, memAddress(framebufferSize), memAddress(framebufferSize) + 4);
			width = framebufferSize.get(0);
			height = framebufferSize.get(1);
		}
		caps = GL.createCapabilities();
		debugProc = GLUtil.setupDebugMessageCallback();
		try (MemoryStack frame = MemoryStack.stackPush()) {
			IntBuffer framebufferSize = frame.mallocInt(2);
			nglfwGetFramebufferSize(window, memAddress(framebufferSize), memAddress(framebufferSize) + 4);
			width = framebufferSize.get(0);
			height = framebufferSize.get(1);
		}
		createTexture();

		createQuadProgram();
		createPointsProgram();

		createSphere();
		loadCoastline();
		loadCities();
		initPlanesPoints();

		thread = new Thread(new SocketThread());
		thread.start();

		glClearColor(0.02f, 0.03f, 0.04f, 1.0f);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glfwShowWindow(window);
	}

	private void initPlanesPoints() {

		vaoPlanes = glGenVertexArrays();
		glBindVertexArray(vaoPlanes);
		planeVbo = glGenBuffers();

		FloatBuffer fb = MemoryUtil.memAllocFloat(10000 * 3);
		fb.flip();

		glBindBuffer(GL_ARRAY_BUFFER, planeVbo);
		glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
		MemoryUtil.memFree(fb);

		glVertexAttribPointer(pointsProgram_inputPosition, 3, GL_FLOAT, false, 3 * 4, 0L);
		glEnableVertexAttribArray(pointsProgram_inputPosition);
		glBindVertexArray(0);

	}

	void createQuadProgram() throws IOException {
		int program = glCreateProgram();
		int vshader = createShader("texturedSphere.vs", GL_VERTEX_SHADER);
		int fshader = createShader("texturedSphere.fs", GL_FRAGMENT_SHADER);
		glAttachShader(program, vshader);
		glAttachShader(program, fshader);
		glLinkProgram(program);
		int linked = glGetProgrami(program, GL_LINK_STATUS);
		String programLog = glGetProgramInfoLog(program);
		if (programLog.trim().length() > 0)
			System.err.println(programLog);
		if (linked == 0)
			throw new AssertionError("Could not link program");
		glUseProgram(program);
		int texLocation = glGetUniformLocation(program, "tex");
		glUniform1i(texLocation, 0);
		sphereProgram_matrixUniform = glGetUniformLocation(program, "matrix");
		sphereProgram_inputPosition = glGetAttribLocation(program, "position");
		sphereProgram_inputTextureCoords = glGetAttribLocation(program, "texCoords");
		glUseProgram(0);
		this.sphereProgram = program;
	}

	void createPointsProgram() throws IOException {

		int program = glCreateProgram();
		int vshader = createShader("points.vs", GL_VERTEX_SHADER);
		int fshader = createShader("points.fs", GL_FRAGMENT_SHADER);
		glAttachShader(program, vshader);
		glAttachShader(program, fshader);
		glLinkProgram(program);
		int linked = glGetProgrami(program, GL_LINK_STATUS);
		String programLog = glGetProgramInfoLog(program);
		if (programLog.trim().length() > 0)
			System.err.println(programLog);
		if (linked == 0)
			throw new AssertionError("Could not link program");
		glUseProgram(program);
		int texLocation = glGetUniformLocation(program, "tex");
		glUniform1i(texLocation, 0);
		pointsProgram_matrixUniform = glGetUniformLocation(program, "matrixPoints");
		pointsProgram_uniPointColor = glGetUniformLocation(program, "uniPointColor");
		pointsProgram_inputPosition = glGetAttribLocation(program, "position");

		glUseProgram(0);
		this.pointsProgram = program;

	}

	void createSphere() {
		vao = glGenVertexArrays();
		glBindVertexArray(vao);
		int vbo = glGenBuffers();
		/* Generate vertex buffer */
		float PI = (float) Math.PI;
		float R = 1f / (rings - 1);
		float S = 1f / (sectors - 1);
		FloatBuffer fb = MemoryUtil.memAllocFloat(rings * sectors * (3 + 2));
		for (int r = 0; r < rings; r++) {
			for (int s = 0; s < sectors; s++) {
				float x = (float) (Math.cos(2 * PI * s * S) * Math.sin(PI * r * R));
				float y = (float) Math.sin(-PI / 2 + PI * r * R);
				float z = (float) (Math.sin(2 * PI * s * S) * Math.sin(PI * r * R));
				fb.put(x).put(y).put(z);
				fb.put(1.0f - s * S).put(1.0f - r * R);
			}
		}
		fb.flip();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
		MemoryUtil.memFree(fb);
		glVertexAttribPointer(sphereProgram_inputPosition, 3, GL_FLOAT, false, (3 + 2) * 4, 0L);
		glEnableVertexAttribArray(sphereProgram_inputPosition);
		glVertexAttribPointer(sphereProgram_inputTextureCoords, 2, GL_FLOAT, false, (3 + 2) * 4, 3 * 4L);
		glEnableVertexAttribArray(sphereProgram_inputTextureCoords);
		/* Generate index/element buffer */
		int ibo = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		IntBuffer ib = MemoryUtil.memAllocInt((rings - 1) * (sectors - 1) * 6);
		for (int r = 0; r < rings - 1; r++) {
			for (int s = 0; s < sectors - 1; s++) {
				ib.put(r * sectors + s).put((r + 1) * sectors + s).put((r + 1) * sectors + s + 1);
				ib.put((r + 1) * sectors + s + 1).put(r * sectors + s + 1).put(r * sectors + s);
			}
		}
		ib.flip();
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ib, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		MemoryUtil.memFree(ib);
		glBindVertexArray(0);
	}

	void createPointsCloud() {
		vaoCoast = glGenVertexArrays();
		glBindVertexArray(vaoCoast);

		int vbo = glGenBuffers();
		FloatBuffer fb = MemoryUtil.memAllocFloat(rings * sectors * (3 + 2));

		for (int i = 0; i < 10000; i++) {
			float lat = (float) ((Math.random() * 360.0) - 180.0);
			float lon = (float) ((Math.random() * 360.0) - 180.0);

			float x = 1.001f * (float) (Math.cos(lat) * Math.cos(lon));
			float y = 1.001f * (float) (Math.cos(lat) * Math.sin(lon));
			float z = 1.001f * (float) Math.sin(lat);

			fb.put(x).put(y).put(z);
		}

		fb.flip();

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
		MemoryUtil.memFree(fb);

		glVertexAttribPointer(pointsProgram_inputPosition, 3, GL_FLOAT, false, 3 * 4, 0L);
		glEnableVertexAttribArray(pointsProgram_inputPosition);
		glBindVertexArray(0);
	}

	void loadCities() throws IOException {
		File citiesCsv = new File(
				Thread.currentThread().getContextClassLoader().getResource("worldcities.csv").getFile());

		BufferedReader bReader = new BufferedReader(new FileReader(citiesCsv));

		bReader.readLine(); // skip header
		String line = null;

		List<Vector2f> coords = new ArrayList<>();

		while ((line = bReader.readLine()) != null) {
			String[] split = line.split(",");
			String strLat = split[2].replaceAll("\"", "").trim();
			String strLon = split[3].replaceAll("\"", "").trim();

			float lat = Float.parseFloat(strLat);
			float lon = Float.parseFloat(strLon);

			coords.add(new Vector2f(lon, lat));
		}

		vaoCities = glGenVertexArrays();
		glBindVertexArray(vaoCities);
		int vbo = glGenBuffers();

		FloatBuffer fb = MemoryUtil.memAllocFloat(coords.size() * 3);

		for (Vector2f coord : coords) {
			Vector3f c = coordsToCartesian(coord, 1.001f);
			fb.put(c.x).put(c.y).put(c.z);
		}

		nbCities = coords.size();

		fb.flip();

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
		MemoryUtil.memFree(fb);

		glVertexAttribPointer(pointsProgram_inputPosition, 3, GL_FLOAT, false, 3 * 4, 0L);
		glEnableVertexAttribArray(pointsProgram_inputPosition);
		glBindVertexArray(0);

	}

	void loadCoastline() throws IOException {
		URL url = Thread.currentThread().getContextClassLoader().getResource("ne_10m_coastline/ne_10m_coastline.shp");

		File shapeFile = new File(url.getFile());
		Map<String, Object> map = new HashMap<>();

		map.put("url", shapeFile.toURI().toURL());
		DataStore dataStore = DataStoreFinder.getDataStore(map);

		SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);

		SimpleFeatureCollection featureCollection = featureSource.getFeatures();
		SimpleFeatureIterator features = featureCollection.features();

		indices = new ArrayList<>();

		vaoCoast = glGenVertexArrays();
		glBindVertexArray(vaoCoast);

		int vbo = glGenBuffers();

		FloatBuffer fb = MemoryUtil.memAllocFloat(410954 * 3);

		features = featureCollection.features();
		int i = 0;
		while (features.hasNext()) {
			SimpleFeature feature = features.next();

			System.out.println(feature.getID());

			Geometry geometry = (Geometry) feature.getDefaultGeometry();
			for (Coordinate coord : geometry.getCoordinates()) {
				float lat = (float) coord.getY();
				float lon = (float) coord.getX();

				Vector3f xyz = coordsToCartesian(lon, lat, 1.001f);
				fb.put(xyz.x).put(xyz.y).put(xyz.z);
				indices.add(i);
				i++;
			}

			indices.add(0xFFFF);
		}

		fb.flip();

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
		MemoryUtil.memFree(fb);

		IntBuffer intBuffer = MemoryUtil.memAllocInt(indices.size());

		for (int idx : indices) {
			intBuffer.put(idx);
		}
		intBuffer.flip();

		eboCoast = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboCoast);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW);

		glVertexAttribPointer(pointsProgram_inputPosition, 3, GL_FLOAT, false, 3 * 4, 0L);
		glEnableVertexAttribArray(pointsProgram_inputPosition);
		glBindVertexArray(0);

	}

	static void createTexture() throws IOException {
		try (MemoryStack frame = MemoryStack.stackPush()) {
			IntBuffer width = frame.mallocInt(1);
			IntBuffer height = frame.mallocInt(1);
			IntBuffer components = frame.mallocInt(1);
			ByteBuffer data = stbi_load_from_memory(ioResourceToByteBuffer("earth.jpg", 1024), width, height,
					components, 4);
			int id = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width.get(), height.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
	}

	void update(float elapsedTime) {
		time += elapsedTime;

		FloatBuffer axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1);

		float x = axes.get(0);
		float y = axes.get(1);

		if (Math.abs(x) > 0.25)
			cameraLon += x;

		if (Math.abs(y) > 0.25)
			cameraLat -= y;

		double deltaX, deltaY;
		if (dragging) {
			deltaX = mouseX - lastMouseX;
			deltaY = mouseY - lastMouseY;

			lastMouseX = (int) mouseX;
			lastMouseY = (int) mouseY;

			cameraLon -= (deltaX / 20.0f);
			cameraLat += (deltaY / 20.0f);
		}

		Vector3f coords = coordsToCartesian(cameraLon, 20.0f, cameraDist);

		Vector3f target = coordsToCartesian(cameraLon, cameraLat, 1.0f);

		m.setPerspective((float) Math.toRadians(5), (float) width / height, 0.1f, 20.0f).lookAt(coords.x, coords.y,
				coords.z, target.x, target.y, target.z, 0, 1, 0);// .rotateY(time / 30.0f);

		mPoints.identity().mul(m);
	}

	void render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Sphere
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glUseProgram(sphereProgram);
		glUniformMatrix4fv(sphereProgram_matrixUniform, false, m.get(matrixBuffer));
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, (rings - 1) * (sectors - 1) * 6, GL_UNSIGNED_INT, 0L);

		// Coasts
		glUseProgram(pointsProgram);
		glUniformMatrix4fv(pointsProgram_matrixUniform, false, mPoints.get(matrixRotBuffer));
		glUniform4f(pointsProgram_uniPointColor, 1.0f, 1.0f, 1.0f, 1.0f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		glEnable(GL_PRIMITIVE_RESTART);
		glPrimitiveRestartIndex(0xFFFF);
		glBindVertexArray(vaoCoast);
		glPointSize(1.0f);
		glDrawElements(GL_LINE_STRIP, indices.size(), GL_UNSIGNED_INT, 0);

		// Cities
		glBindVertexArray(vaoCities);
		glPointSize(2.0f);
		glUniform4f(pointsProgram_uniPointColor, 1.0f, 0.0f, 0.0f, 0.1f);
		glDrawArrays(GL_POINTS, 0, nbCities);

		// Planes
		/*
		 * glBindVertexArray(vaoPlanes);
		 * 
		 * glBindBuffer(GL_ARRAY_BUFFER, planeVbo);
		 * 
		 * int nbPlanePoints = planesPoints.size();
		 * 
		 * while(planeIdx < nbPlanePoints){
		 * 
		 * Vector3f coord = coordsToCartesian(planesPoints.get(planeIdx), 1.2f);
		 * 
		 * glBufferSubData(GL_ARRAY_BUFFER, planeIdx * (3*4) , new float[] { coord.x,
		 * coord.y, coord.z}); planeIdx++; }
		 * 
		 * glPointSize(1.0f); glUniform4f(pointsProgram_uniPointColor, 0.0f, 1.0f, 0.0f,
		 * 1.0f); glDrawArrays(GL_POINTS, 0, nbPlanePoints);
		 * 
		 * glBindVertexArray(0); glUseProgram(0);
		 */
	}

	void loop() {
		long lastTime = System.nanoTime();
		while (!glfwWindowShouldClose(window)) {
			glfwPollEvents();
			glViewport(0, 0, width, height);
			long thisTime = System.nanoTime();
			float elapsedTime = (thisTime - lastTime) * 1E-9f;
			lastTime = thisTime;
			update(elapsedTime);
			render();
			glfwSwapBuffers(window);
		}
	}

	void run() throws IOException {
		init();
		loop();
		glfwDestroyWindow(window);
		glfwTerminate();
		thread.interrupt();
		/* Free memory resources */
		if (debugProc != null)
			debugProc.free();
		keyCallback.free();
		fbsCallback.free();
		GL.setCapabilities(null);
		MemoryUtil.memFree(matrixBuffer);
	}

	public static void main(String[] args) throws IOException {
		new Carto3D().run();
	}

}
