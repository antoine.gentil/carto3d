package antgent.carto3d;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.MultiLineString;
import org.opengis.feature.GeometryAttribute;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ShapefileTest {

    public static void main(String[] args) throws Exception {

        URL url = Thread.currentThread().getContextClassLoader().getResource("ne_10m_coastline/ne_10m_coastline.shp");

        File shapeFile = new File(url.getFile());
        Map<String, Object> map = new HashMap<>();

        map.put("url", shapeFile.toURI().toURL());
        DataStore dataStore = DataStoreFinder.getDataStore(map);

        SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);

        SimpleFeatureCollection featureCollection = featureSource.getFeatures();
        SimpleFeatureIterator features = featureCollection.features();

        while(features.hasNext()){
            SimpleFeature feature = features.next();

            System.out.println(feature.getID());

            Geometry geometry = (Geometry) feature.getDefaultGeometry();
            for(Coordinate coord : geometry.getCoordinates()){
                System.out.println(coord.getX()+ " " +coord.getY());
            }
        }

    }

}
